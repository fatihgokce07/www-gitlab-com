---
layout: job_page
title: "Account Executive (EMEA & US)"
---

## Time zone

* We are looking for candidates to be located in the US and also northern Europe or able to support that time zone.

## Responsibilities

* Account Executive (AE) will report to the CRO or a regional director over the course of the year
* Act as a primary point of contact and the face of GitLab for our prospects.
* Contribute to post-mortem analysis on wins/losses.
   * Communicate lessons learned to the team, including account managers, the marketing team, and the technical team.
* Take ownership of your book of business
    * document the buying criteria
    * document the buying process
    * document next steps and owners
    * ensure pipeline accuracy based on evidence and not hope
* Contribute to documenting improvements in our [sales handbook](https://about.gitlab.com/handbook/sales/).
* Provide account leadership and direction in the pre- and post-sales process
* Be the voice of the customer by contributing product ideas to our public [issue tracker](https://gitlab.com/gitlab-org/gitlab-ee/issues)

## Requirements

* A true desire to see customers benefit from the investment they make with you
* 2+ years of experience with B2B sales
* Interest in GitLab, and open source software
* Ability to leverage established relationships and proven sales techniques for success
* Effective communicator, strong interpersonal skills
* Motivated, driven and results oriented
* Excellent negotiation, presentation and closing skills
* Preferred experience with Git, Software Development Tools, Application Lifecycle Management
* You share our [values](/handbook/values), and work in accordance with those values.

## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).

* Qualified applicants receive a short questionnaire from our Global Recruiters
  1. What would differentiate you and make you a great account executive for GitLab?
  1. What is your knowledge of the space that GitLab is in? (i.e. Industry Trends)
  1. How do you see the developer tools changing over the coming years from a sales perspective? (i.e. Competitive Positioning, Customer Needs, etc)
* Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with Sales Director - US
* Candidates will then be invited to schedule a second interview with Sales Director - EMEA
* Candidates will be invited to schedule a third interview with our CRO
* Finally, candidates will interview with our CEO
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Compensation

Your On Target Earnings (OTE) compensation is equal to `2 x quota x commission rate (7% without the accelerator)`.
You will typically get 50% as base and 50% based on commission. See our
[market segmentation](https://about.gitlab.com/handbook/sales/#market-segmentation) for
typical quotas in the U.S. As an example, a strategic account manager in the US
with a $1.5M quota will have an OTE of $210,000 and a base salary of $105,000.
Also see the [Sales Compensation Plan](https://about.gitlab.com/handbook/finance/sales-comp-plan/).
